# Introduction

These are the instructions for getting and installing the OVJ
version of OPTO. If you are looking for the TopSpin version
please go here: [README-TOPSPIN.md](/README-TOPSPIN.md)

# System requirements
  
    Software
    
    - A computer running an OS that will run OpenVNMRJ (newer than 3.1A)
    
        The OpenVNMRJ project is here: https://github.com/OpenVnmrJ/OpenVnmrJ
      
        The project README for 3.2A says supported OSes include:
        
          AlmaLinux: Version 8 and 9.
          RHEL: Version 8 and 9.
          Ubuntu: Version 20, 22, and 24.
        
        We have tested on:
        
          Ubuntu 20.04

    - An installation of OVJ newer than 3.1A
    
        To learn how to install OVJ see https://github.com/OpenVnmrJ/OpenVnmrJ/releases
      
        We have tested on:
      
          OVJ 3.1A with custom code changes in vnmrsys

            (The current master branch of OVJ already includes our custom code changes)

          OVJ 3.2A

            (OVJ 3.2A already contains our custom code changes as well)
        
        It is possible it will work with old Agilent versions
          of VNMRJ but they have not yet been tested.
      
    - A java JRE runtime version 1.8 or later installed on the host computer
      
        We have tested it with:
        
          OpenJDK 1.8
          OpenJDK 11

    Hardware

    - A Varian (Agilent) spectrometer / magnet combo
      
        Attached to the host computer
  
# Installation guide

    Total time spent installing will be a few minutes
    
    The public facing OVJ project web page is here:
    
      https://git.doit.wisc.edu/nmrfam-public/opto-releases/-/blob/main/README-OVJ.md
    
    Download the opto-files.zip file into your home directory as ~/opto-files.zip
    
      The file url is https://git.doit.wisc.edu/nmrfam-public/opto-releases/-/blob/main/ovj-files/opto-files.zip
    
    Download the opto-ovj.zip installer package into your home directory
      as ~/opto-ovj.zip
      
      The file url is https://git.doit.wisc.edu/nmrfam-public/opto-releases/-/blob/main/ovj-files/opto-ovj.zip
    
    Extract opto-ovj.zip under your home directory
    
      mkdir ~/opto-tmp
      cd ~/opto-tmp
      unzip ../opto-ovj.zip
     
    run the install program
     
      sudo ./install.sh
      
      (You will be asked for a password. The OPTO install process requires
       sudo access by the logged in user to the machine in order to work.)

    At this point the opto.sh executable script and supporting installation
    files are installed in /vnmr/bin
    
    You can delete the ~/opto-tmp directory and all its contents
    
    After the opto program has been installed unzip the opto-files.zip
    file into ~/vnmrsys/opto. If the directory does not already exist then
    please make it before unzipping. This directory will then hold
    startup files and job files etc. that are required by Opto.
    
# Uninstall guide

  If OPTO has already been installed there is an uninstall-opto.sh
  executable script in the path. Simply run 'sudo uninstall-opto.sh' and
  OPTO files will be removed from the installation directories. Your OPTO
  job files and setting files will remain.
      
# Demo

  Demonstration data is shown within the main text of the following publication: 
  
  Example files are provided within the /opto-releases/ovj-files/opto-files.zip file.
  These files contain .dat and .prm.job files that can be used to set up
  and test parameter optimizations for Varian experiments. 

# Instructions for use
  
  The OPTO manual is provided as a PDF for the OpenVnmrJ version here:
  
    https://git.doit.wisc.edu/nmrfam-public/opto-releases/-/blob/main/ovj-files/OPTO_OVJ_Manual.pdf

  Launch OpenVnmrJ
  
  In the command entry field inside OVJ type "listenon"
  
  From a terminal run opto.sh (it is already in your path)
  
  Opto will launch and start communicating with OVJ
    
    The Opto gui allows one to build jobs and run them

    In the Opto gui define your job and the press the Run button
   
      To learn about jobs and how to build them see the manual:
      
        https://git.doit.wisc.edu/nmrfam-public/opto-releases/-/blob/main/ovj-files/OPTO_OVJ_Manual.pdf
          
    The terminal window will show output progress from the
      running Opto job.
    
    When running an optimization log files are written to the current
      experiment directory with names starting with "OPTO-*". These can
      be useful for figure creation and for checking the accuracy of the
      results.

# Project site

  The public facing OPTO project page is [HERE](/README.md)
  
  Go there to read more about the OPTO project on all platforms, etc.

# LICENSE

  Copyright UW-Madison Board of Regents (2022-present). All rights reserved.

  The Regents of the University of Wisconsin-Madison hereby grant the end user an internal, non-exclusive, non-commercial license to NMRFAM software for academic and scholarly purposes only. All rights reserved to the Regents of the University of Wisconsin-Madison. The end user agrees and acknowledges that redistribution to third parties not covered by this license is explicitly prohibited.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  For non-academic use, please contact Justin Anderson at the Wisconsin Alumni Research Foundation (janderson@warf.org) to obtain a commercial license.
  
# How to cite

  OPTO: Automated Optimization for Solid-State NMR Spectroscopy
 
    Collin G. Borcik, Barry DeZonia, Thirupathi Ravula, Benjamin D. Harding, Rajat Garg, Chad M. Rienstra

    Journal of the American Chemical Society

    January 15, 2025

  Article link: https://pubs.acs.org/doi/full/10.1021/jacs.4c13295 

  DOI 10.1021/jacs.4c13295

# DOI of software version(s)

  TODO : Zenodo?
