# Information

opto-releases is the repository that contains info
on the software releases of the OPTO program.

OPTO is optimization software used by NMR scientists
to drive either OpenVNMRJ or TopSpin in order to find
better signal profiles for a given experiment. It can
optimize shim values and experimental parameter values.

OPTO was designed by the team at NMRFAM at the
University of Wisconsin at Madison under the supervision
of Chad Rienstra. Development began in 2022.

Literature relating to OPTO can be found here:

  [Initial article in JACS](https://pubs.acs.org/doi/full/10.1021/jacs.4c13295)

For more information about the the OVJ version of OPTO
go here:

  [README-OVJ.md](/README-OVJ.md)

For more information about the the TopSpin version of OPTO
go here:

  [README-TOPSPIN.md](/README-TOPSPIN.md)
