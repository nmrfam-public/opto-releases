# Introduction

These are the instructions for getting and installing the TopSpin
version of Opto. If you are looking for the OVJ version please
go here: [README-OVJ.md](/README-OVJ.md)

# System Requirements
  
    Software
    
    - A computer running an OS that will run TopSpin 3.6.x or later
    
        Read about TopSpin HERE:
        
          https://www.bruker.com/en/products-and-solutions/mr/nmr-software/topspin.html
      
        That site says supported OSes include:
        
          AlmaLinux 9.2
          CentOS 7
          Windows 10
          Windows 11.
          
        We have tested on:
        
          AlmaLinux 9.X
          CentOS 7

        Note that Opto Topspin does not yet support Windows
            
    - An installation of TopSpin 3.6.x or later
    
        To learn how to install TopSpin please visit the Bruker website
      
          https://www.bruker.com

        We have tested on:
      
          TopSpin 3.6.X
          TopSpin 4.3.0
          TopSpin 4.4.x
        
        It is possible it will work with other as yet untested versions
          of TopSpin.
      
    - A java JRE runtime version 1.8 or later installed on the host computer
      
        We have tested it with:
        
          OpenJDK 1.8

    Hardware

    - A Bruker spectrometer / magnet combo
      
        Attached to the host computer
  
# Installation guide

    Total time spent installing will be a few minutes
    
    The public facing Opto TopSpin project web page is here:
    
      https://git.doit.wisc.edu/nmrfam-public/opto-releases/-/blob/main/README-TOPSPIN.md
    
    Download the opto-topspin.zip installer package into your home
      directory as ~/opto-topspin.zip
      
      The file url is https://git.doit.wisc.edu/nmrfam-public/opto-releases/-/blob/main/topspin-files/opto-topspin.zip
    
    Extract it under your home directory
    
      mkdir ~/opto-tmp
      cd ~/opto-tmp
      unzip ../opto-topspin.zip
     
    run the install program
     
      sudo ./install.sh <topspin version>
      
      (You will be asked for a password. The Opto install process requires
       sudo access by the logged in user to the machine in order to work.)
      
      <topspin version> can be names like topspin3.6.5 etc.
      It is the name of the installation directory under /opt
      where the versions of TopSpin reside.

    At this point the opto.sh executable script and supporting installation
    files are installed in /opt/<topspin version>
    
    You can delete the ~/opto-tmp directory and all its contents

# Uninstall guide

  If Opto has already been installed there is an uninstall-opto.sh
  executable script in the path. Simply run
  'sudo uninstall-opto.sh \<topspin version\>' and Opto files will be
  removed from the installation directories. Your Opto job files and
  setting files will remain.
    
# Demo

  Due to hardware prerequisites it is not possible to provide a demo

  TODO - do we provide a .job file and a pulse sequence .c file and any
    associated setup macros as a demo?

# Instructions for use
  
    Launch TopSpin
    
    Run the command open-opto.py from within TopSpin

      TopSpin will start listening and will launch the Opto GUI
    
    The Opto gui allows one to build jobs and run them

    In the Opto gui define your job and the press the Run button
   
      To learn about jobs and how to build them see the manual:
      
        https://git.doit.wisc.edu/nmrfam-public/opto-releases/-/blob/main/topspin-files/OPTO_Topspin_Manual.pdf
          
    The terminal window will show output progress from the
      running Opto job.
    
    When running an optimization log files are written to the current
      experiment directory with names starting with "OPTO-*". These can
      be useful for figure creation and for checking the accuracy of the
      results.

# Project site

  The public facing Opto project page is [HERE](/README.md)
  
  Go there to read more about the Opto project on all platforms, etc.

# LICENSE

  Copyright UW-Madison Board of Regents (2022-present). All rights reserved.

  The Regents of the University of Wisconsin-Madison hereby grant the end user an internal, non-exclusive, non-commercial license to NMRFAM software for academic and scholarly purposes only. All rights reserved to the Regents of the University of Wisconsin-Madison. The end user agrees and acknowledges that redistribution to third parties not covered by this license is explicitly prohibited.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  For non-academic use, please contact Justin Anderson at the Wisconsin Alumni Research Foundation (janderson@warf.org) to obtain a commercial license.
  
# How to cite

  OPTO: Automated Optimization for Solid-State NMR Spectroscopy
  
    Collin G. Borcik, Barry DeZonia, Thirupathi Ravula, Benjamin D. Harding, Rajat Garg, Chad M. Rienstra

    Journal of the American Chemical Society

    January 15, 2025

  Article link: https://pubs.acs.org/doi/full/10.1021/jacs.4c13295   

  DOI 10.1021/jacs.4c13295

# DOI of software version(s)

  TODO - Zenodo?
